Ansible Role: GitLab Omnibus
=========

This role installs GitLab Omnibus.

[![Build Status](https://travis-ci.org/CyVerse-Ansible/ansible-role-template.svg?branch=master)](https://gitlab.com/sandlin/ansible-galaxy/omnibus)
[![Ansible Galaxy](https://img.shields.io/badge/ansible--galaxy-name--of--my--role-blue.svg)](https://galaxy.ansible.com/CyVerse-Ansible/ansible-role-template/)

The default administrator username is `root`.
The default administrator password is `5iveL!fe`.

Requirements
------------

GitLab Enterprise requires a license.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

| Variable                | Required | Default   | Choices                   | Comments                                 |
|-------------------------|----------|-----------|---------------------------|------------------------------------------|
| gitlab_root_email       | yes      | root      |                           | |
| gitlab_root_password    | yes      | 5iveL!fe  |                           | |
| api_username            | yes      | None      |                           | |
| api_password            | yes      | None      |                           | |
| external_url            | yes      | None      |                           | |
| smtp_enabled            | yes      | False     |                           | |
| email_enabled           | yes      | False     |                           | |
| ldap_enabled            | yes      | False     |                           | |
| gitlab_edition          | yes      | None      | ce, ee                    | |
| gitlab_version          | yes      | None      | 13.8.1                    | ex: 13.8.1                                |
| self_signed_cert_subj   | no       | None      |                           | |
| license_key             | no       | None      |                           | |
| time_zone               | no       | UTC       |                           | |




Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
         - omnibus

License
-------

BSD

Author Information
------------------

[James Sandlin](mailto:jsandlin@gitlab.com)
